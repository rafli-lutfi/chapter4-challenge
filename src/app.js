require("dotenv").config()

const express = require("express")
const logger = require("morgan")
const router = require("./routes")

const port = process.env.PORT

const app = express()

app.use(logger("tiny"))
app.use(express.json())

app.use("/api/v1", router)

// Handle 404
app.use((req,res,next)=>{
  return res.status(404).json({
    status: false,
    message: "page not found",
    data: null
  })
})

// Handle 500
app.use((err, req, res, next) =>{
  console.log(err);
  return res.status(500).json({
    status: false,
    message: err.message,
    data: null
  })
})


app.listen(port, () =>{
  console.log(`listening on port:${port}`);
})