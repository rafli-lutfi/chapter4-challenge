const router = require("express").Router()
const {product_component}  = require("../controllers")

router.post("/add", product_component.add)
router.delete("/remove", product_component.remove)

module.exports = router