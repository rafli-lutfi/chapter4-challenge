const router = require("express").Router()
const {component_supplier} = require("../controllers")

router.post("/create", component_supplier.create)
router.delete("/delete", component_supplier.delete)

module.exports = router