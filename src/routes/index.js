const router = require("express").Router()

const supplier = require("./supplier")
const component = require("./component")
const product = require("./product")
const componentSupplier = require("./component_supplier")
const productComponent = require("./product_component")

router.use("/suppliers", supplier)
router.use("/components", component)
router.use("/componentSupplier", componentSupplier)
router.use("/products", product)
router.use("/productComponents", productComponent)


module.exports = router