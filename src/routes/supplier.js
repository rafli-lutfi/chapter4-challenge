const router = require("express").Router()
const {supplier} = require("../controllers")

router.get("/", supplier.getAll)
router.get("/:supplier_id", supplier.getDetail)
router.post("/", supplier.create)
router.put("/supplier_id", supplier.update)
router.delete("/", supplier.delete)

module.exports = router