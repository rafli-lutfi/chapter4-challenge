const router = require("express").Router()
const {product} = require("../controllers")

router.get("/", product.getAll)
router.get("/:product_id", product.getDetail)
router.post("/", product.create)
router.put("/:product_id", product.update)
router.delete("/", product.delete)

module.exports = router