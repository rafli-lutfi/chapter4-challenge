const router = require("express").Router()
const {component} = require("../controllers")

router.get("/", component.getAll)
router.get("/:component_id", component.getDetail)
router.post("/", component.create)
router.put("/:component_id", component.update)
router.delete("/", component.delete)

module.exports = router