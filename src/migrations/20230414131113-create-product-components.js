'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('product_components', {
      product_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        onDelete: "CASCADE",
        references:{
          model: "products",
          key: "id",
          as: "product_id"
        }
      },
      component_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        onDelete: "CASCADE",
        references:{
          model: "components",
          key: "id",
          as: "component_id"
        }
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('product_components');
  }
};