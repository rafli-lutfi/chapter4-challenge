'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class product_components extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.products, {
        foreignKey: "product_id",
        onDelete: "CASCADE",
      }),
      this.belongsTo(models.components, {
        foreignKey: "component_id",
        onDelete: "CASCADE",
      })
    }
  }
  product_components.init({
    product_id: DataTypes.INTEGER,
    component_id: DataTypes.INTEGER
  }, {
    sequelize,
    timestamps: false,
    modelName: 'product_components',
  });

  product_components.removeAttribute("id");

  return product_components;
};