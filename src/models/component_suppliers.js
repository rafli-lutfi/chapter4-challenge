'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class component_suppliers extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.components, {
        foreignKey: "component_id",
        onDelete: "CASCADE",
      })
      this.belongsTo(models.suppliers, {
        foreignKey: "supplier_id",
        onDelete: "CASCADE",
      })
    }
  }
  component_suppliers.init({
    component_id: DataTypes.INTEGER,
    supplier_id: DataTypes.INTEGER
  }, {
    sequelize,
    timestamps: false,
    modelName: 'component_suppliers',
  });

  component_suppliers.removeAttribute("id");
  
  return component_suppliers;
};