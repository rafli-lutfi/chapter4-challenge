const { components: Component, suppliers: Supplier, component_suppliers: ComponentSupplier } = require("../models");

module.exports = {
  getAll: async (req, res, next) => {
    try {
      const components = await Component.findAll({ order: [["id", "ASC"]] });

      res.status(200).json({
        status: true,
        message: "success get all component",
        data: components,
      });
    } catch (err) {
      next(err);
    }
  },
  getDetail: async (req, res, next) => {
    try {
      const { component_id } = req.params;

      if (!component_id) {
        throw new Error("missing component id");
      }

      const component = await Component.findOne({ where: { id: component_id } });
      if (!component) {
        throw new Error("component not found");
      }

      const component_suppliers = await ComponentSupplier.findAll({
        where: { component_id: component_id },
        include: Supplier,
      });

      const suppliers = component_suppliers.map((element) => {
        return element.supplier;
      });

      return res.status(200).json({
        status: true,
        message: "success get detail component",
        data: {
          component: component,
          suppliers: suppliers,
        },
      });
    } catch (err) {
      if (err == "missing component id" || err == "component not found") {
        return res.status(400).json({
          status: false,
          message: err.message,
          data: null,
        });
      }
      next(err);
    }
  },
  create: async (req, res, next) => {
    try {
      const { name, description, supplier_id } = req.body;

      if (!name || !description) {
        throw new Error("missing data request body");
      }

      if (supplier_id) {
        if (typeof supplier_id != "object") {
          throw new Error("supllier id must be an array");
        }

        const checkSupplier = await Supplier.findAndCountAll({ where: { id: supplier_id } });

        if (checkSupplier.count != supplier_id.length) {
          throw new Error("there's supplier id not found");
        }
      }

      const newComponent = await Component.create({ name: name, description: description });
      const component_id = newComponent.id;

      let addMessage = "";
      if (supplier_id) {
        const data = supplier_id.map((id) => {
          return { component_id: component_id, supplier_id: id };
        });

        await ComponentSupplier.bulkCreate(data);

        addMessage += " and create new record in table component_suppliers";
      }

      return res.status(201).json({
        status: true,
        message: `Success create new component${addMessage}`,
        data: newComponent,
      });
    } catch (err) {
      if (err == "missing data request body" || err == "supplier id not found") {
        return res.status(400).json({
          status: false,
          message: err.message,
          data: null,
        });
      }
      next(err);
    }
  },
  update: async (req, res, next) => {
    try {
      const { component_id } = req.params;
      const { name, description } = req.body;

      if (!component_id) {
        throw new Error("missing component id");
      }

      console.log(name, description);
      if (!name && !description) {
        throw new Error("request body is empty");
      }

      const checkComponent = await Component.findOne({ where: { id: component_id } });
      if (!checkComponent) {
        throw new Error("component not found");
      }

      const updatedComponent = await Component.update(
        {
          name: name,
          description: description,
        },
        { where: { id: component_id } }
      );

      return res.status(200).json({
        status: true,
        message: `success updated component id ${component_id}`,
        data: null,
      });
    } catch (err) {
      if (err == "missing component id" || err == "request body is empty" || err == "component not found") {
        return res.status(400).json({
          status: false,
          message: err.message,
          data: null,
        });
      }
      next(err);
    }
  },
  delete: async (req, res, next) => {
    try {
      const { component_id } = req.query;

      if (!component_id) {
        throw new Error("missing component_id param");
      }

      const checkComponent = await Component.findOne({ where: { id: component_id } });
      if (!checkComponent) {
        throw new Error("component not found");
      }

      await Component.destroy({ where: { id: component_id } });

      return res.status(200).json({
        status: true,
        message: `success delete component id ${component_id}`,
        data: null,
      });
    } catch (err) {
      if (err == "missing component_id param" || err == "component not found") {
        return res.status(400).json({
          status: false,
          message: err.message,
          data: null,
        });
      }
      next(err);
    }
  },
};
