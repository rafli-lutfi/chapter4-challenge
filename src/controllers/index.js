const supplier = require("./supplier");
const component = require("./component");
const product = require("./product");
const component_supplier = require("./component_supplier");
const product_component = require("./product_component");

module.exports = { supplier, component, component_supplier, product, product_component };
