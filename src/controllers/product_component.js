const { products: Product, components: Component, product_components: ProductComponents } = require("../models");

module.exports = {
  add: async (req, res, next) => {
    try {
      const { product_id } = req.query;
      const { component_id } = req.body;

      if (!product_id || !component_id) throw new Error("missing id in parameter or body");

      if (typeof component_id != "object") throw new Error("component_id must be an array");

      const checkComponent = await Component.findAndCountAll({ where: { id: component_id } });

      if (checkComponent.count != component_id.length) throw new Error("there's is some id that cant be found");

      const checkProduct = await Product.findOne({ where: { id: product_id } });

      if (!checkProduct) throw new Error("product not found");

      const data = component_id.map((id) => {
        return { product_id: product_id, component_id: id };
      });

      const checkProductComponent = await ProductComponents.findAndCountAll({ where: data });

      if (checkProductComponent.count == product_id.length) throw new Error("there's some data that already exist");

      await ProductComponents.bulkCreate(data);

      res.status(201).json({
        status: false,
        message: "success create records in table product_components",
        data: null,
      });
    } catch (err) {
      if (
        err == "missing id in parameter or body" ||
        err == "component_id must be an array" ||
        err == "there's is some id that cant be found" ||
        err == "product not found" ||
        err == "there's some data that already exist"
      ) {
        return res.status(400).json({
          status: false,
          message: err.message,
          data: null,
        });
      }
      next(err);
    }
  },
  remove: async (req, res, next) => {
    try {
      const {product_id, component_id} = req.query

      if(!product_id || !component_id) throw new Error("missing product or component id")

      const deletedItems = await ProductComponents.destroy({where:{product_id:product_id ,component_id: component_id}})
      console.log(deletedItems);
      if(!deletedItems) throw new Error("no record found in table product_components")

      return res.status(200).json({
        status: true,
        message: "success deleted record in table product_components",
        data: null
      })
    } catch (err) {
      if(err == "missing product or component id" || err == "product not found" || err == "component not found" || err == "no record found in table product_components"){
        return res.status(400).json({
          status: false,
          message: err.message,
          data: null
        })
      }
      next(err)
    }
  },
};
