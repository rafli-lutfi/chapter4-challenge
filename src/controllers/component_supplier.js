const { suppliers: Supplier, components: Component, component_suppliers: ComponentSupplier } = require("../models");

module.exports = {
  create: async (req, res, next) => {
    try {
      const { supplier_id } = req.query;
      const { component_id } = req.body;

      if (!supplier_id || !component_id) {
        throw new Error("missing supplier id or component id");
      }

      if (typeof component_id != "object") {
        throw new Error("component id must be an array");
      }

      const checkComponent = await Component.findAndCountAll({ where: { id: component_id } });

      if (checkComponent.count != component_id.length) {
        throw new Error("there's component id that can't be found");
      }

      const checkSupplier = Supplier.findOne({ where: { id: supplier_id } });
      if (!checkSupplier) {
        throw new Error("supplier not found");
      }

      const data = component_id.map((id) => {
        return { component_id: id, supplier_id: supplier_id };
      });

      const checkComponentSupplier = await ComponentSupplier.findAndCountAll({ where: { component_id: component_id, supplier_id: supplier_id } });

      if (checkComponentSupplier.count == component_id.length) {
        throw new Error("there is some data that already exist");
      }

      await ComponentSupplier.bulkCreate(data);

      return res.status(200).json({
        status: true,
        message: "success create record/records in table component_supplier",
        data: null,
      });
    } catch (err) {
      if (err == "missing supplier id or component id" || err == "component id must be an array" || err == "there's component id that can't be found" || err == "supplier not found") {
        return res.status(400).json({
          status: false,
          message: err.message,
          data: null,
        });
      }
      next(err);
    }
  },
  delete: async (req, res, next) => {
    try {
      const { supplier_id, component_id } = req.query;

      if (!supplier_id || !component_id) {
        throw new Error("missing id in parameter or query");
      }

      const checkSupplier = await Supplier.findOne({ where: { id: supplier_id } });
      if (!checkSupplier) {
        throw new Error("supplier not found");
      }

      const checkComponent = await Component.findOne({ where: { id: component_id } });
      if (!checkComponent) {
        throw new Error("component not found");
      }

      const checkComponentSupplier = await ComponentSupplier.findOne({ where: { supplier_id: supplier_id, component_id: component_id } });
      if (!checkComponentSupplier) {
        throw new Error("record not found");
      }

      await ComponentSupplier.destroy({ where: { component_id: component_id, supplier_id: supplier_id } });

      return res.status(200).json({
        status: true,
        message: "success delete records in table component_supplier",
        data: null,
      });
    } catch (err) {
      if (err == "missing id in parameter or query" || err == "supplier not found" || err == "component not found") {
        return res.status(400).json({
          status: false,
          message: err.message,
          data: null,
        });
      }
      next(err);
    }
  },
};
