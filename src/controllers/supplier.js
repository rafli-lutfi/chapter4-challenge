const { suppliers: Supplier, components: Component, component_suppliers: ComponentSupplier } = require("../models");

module.exports = {
  getAll: async (req, res, next) => {
    try {
      const suppliers = await Supplier.findAll({
        order: [["id", "ASC"]],
      });

      return res.status(200).json({
        status: true,
        message: "success get all suppliers",
        data: suppliers,
      });
    } catch (err) {
      next(err);
    }
  },
  getDetail: async (req, res, next) => {
    try {
      const { supplier_id } = req.params;

      if (!supplier_id) {
        throw new Error("missing query param");
      }

      const supplier = await Supplier.findOne({ where: { id: supplier_id } });

      if (!supplier) {
        throw new Error("supplier not found");
      }

      const component_suppliers = await ComponentSupplier.findAll({ where: { supplier_id: supplier_id }, include: Component });

      const components = component_suppliers.map((element) => {
        return element.component;
      });

      res.status(200).json({
        status: true,
        message: "success get detail suppliers",
        data: {
          supplier: supplier,
          components: components,
        },
      });
    } catch (err) {
      if (err == "missing query param" || err == "supplier not found") {
        return res.status(400).json({
          status: false,
          message: err.message,
          data: null,
        });
      }
      next(err);
    }
  },
  create: async (req, res, next) => {
    try {
      const { name, address, component_id } = req.body;

      if (!name || !address) {
        throw new Error("missing data request body");
      }

      // component id is an optional input, and it will be validated if included in request body
      if (component_id) {
        if (typeof component_id != "object") {
          throw new Error("component id must be an array");
        }

        const checkComponent = await Component.findAndCountAll({ where: { id: component_id } });

        if (checkComponent.count != component_id.length) {
          throw new Error("there's id not found");
        }
      }

      const newSupplier = await Supplier.create({ name: name, address: address });

      const supplier_id = newSupplier.id;

      let addMessage = "";
      if (component_id) {
        const data = component_id.map((id) => {
          return { component_id: id, supplier_id: supplier_id };
        });

        await ComponentSupplier.bulkCreate(data);

        addMessage += " and create records in table component_suppliers";
      }

      return res.status(201).json({
        status: true,
        message: `success create new supplier${addMessage}`,
        data: newSupplier,
      });
    } catch (err) {
      if (err == "missing data request body" || err == "component id must be an array" || err == "component id not found") {
        // handle Bad Request 400
        return res.status(400).json({
          status: false,
          message: err.message,
          data: null,
        });
      }
      next(err);
    }
  },
  update: async (req, res, next) => {
    try {
      const { supplier_id } = req.params;
      const { name, address } = req.body;

      if (!supplier_id) {
        throw new Error("missing supplier id");
      }

      if (!name && !address) {
        throw new Error("missing data body request");
      }

      const checkSupplier = await Supplier.findOne({ where: { id: supplier_id } });
      if (!checkSupplier) {
        throw new Error("supplier not found");
      }

      await Supplier.update({ name: name, address: address }, { where: { id: supplier_id } });

      return res.status(200).json({
        status: true,
        message: `success update supplier id:${supplier_id}`,
        data: null,
      });
    } catch (err) {
      if (err == "missing supplier id" || err == "missing data body request" || err == "supplier not found") {
        return res.status(400).json({
          status: false,
          message: err.message,
          data: null,
        });
      }
      next(err);
    }
  },
  delete: async (req, res, next) => {
    try {
      const { supplier_id } = req.query;

      if (!supplier_id) {
        throw new Error("missing supplier id query");
      }

      const checkSupplier = await Supplier.findOne({ where: { id: supplier_id } });
      if (!checkSupplier) {
        throw new Error("supplier not found");
      }

      Supplier.destroy({ where: { id: supplier_id } });

      return res.status(200).json({
        status: true,
        message: `success deleted supplier id ${supplier_id}`,
        data: null,
      });
    } catch (err) {
      if (err == "missing supplier id params" || err == "supplier not found") {
        return res.status(400).json({
          status: false,
          message: err.message,
          data: null,
        });
      }
      next(err);
    }
  }
};
