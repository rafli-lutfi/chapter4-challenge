# Chapter 4 Challenge

## Setup Database
> don't run these script in **/src** folder. 
### create database: 
`sequelize db:create --config src/config/config.js`

### migrate database:
`sequelize db:migrate --config src/config/config.js --migrations-path src/migrations`

### insert all data dummy:
`sequelize db:seed:all --config src/config/config.js --seeders-path src/seeders`

### insert specific data dummy:
`sequelize db:seed --seed <filename.js> --config src/config/config.js --seeders-path src/seeders`
